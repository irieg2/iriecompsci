# Name:

# See the README file for instructions.

from main import TurnOn
from main import TurnOff
from main import TurnLeft
from main import Move
from main import PickUp
from main import PutDown

from main import IsAtPickUp
from main import IsAtDeposit
from main import IsAtEnd
from main import IsClearAhead
from main import IsClearLeft
from main import IsClearRight
from main import HasPickUps
from main import HasError


from glob import UP
from glob import RIGHT
from glob import LEFT
from glob import DOWN

# This can be any value 0-7.  Chane it to test each function.
CURRENT_GAME = 7

# Set this to False to just test all your results without a GUI.  When set to True
# it will test one puzzle at a time.
SHOW_GUI = False

# The number of miliseconds between each step.  Raise it to slow things down.
# Lower it to speed it up.
STEP_TIME = 500

def TurnRight():
    TurnLeft()
    TurnLeft()
    TurnLeft()

def TurnAround():
    TurnLeft()
    TurnLeft()

def MoveNumber(n):
    for i in xrange(n):
        if IsAtPickUp() == True:
            PickUp()

        
            
        if IsClearAhead() == False:
            if IsClearLeft() == False and IsClearRight() == False:
                TurnAround()
            if IsClearLeft() == False and IsClearRight() == True:
                TurnRight()
            if IsClearLeft() == True and IsClearRight() == False:
                TurnLeft()
                
        while HasPickUps() == True and IsAtDeposit() == True:
            PutDown()  
        Move()
        
        if IsAtEnd() == True:
            TurnOff()
            break

def SingleDrop():
    Move()
    PutDown()
          
def Solve0():
    TurnOn()
    TurnAround()
    Move()
    TurnLeft()
    MoveNumber(2)
    TurnOff()
    
    

def Solve1():
    TurnOn()
    TurnRight()
    MoveNumber(2)
    TurnRight()
    MoveNumber(1)
  

def Solve2():
    TurnOn()
    MoveNumber(2)
    TurnLeft()
    MoveNumber(3)
    TurnRight()
    MoveNumber(6)


def Solve3():
    TurnOn()
    MoveNumber(7)
    TurnAround()
    MoveNumber(7)
    TurnLeft()
    MoveNumber(2)
    TurnLeft()
    MoveNumber(1)


def Solve4():
    TurnOn()
    MoveNumber(4)
    TurnRight()
    MoveNumber(5)
    TurnRight()
    MoveNumber(3)


def Solve5():
    TurnOn()
    for n in xrange(5):
        MoveNumber(1)
        TurnRight()
        MoveNumber(1)
        TurnLeft()
    MoveNumber(1)
    TurnRight()
    MoveNumber(1)


def Solve6():
    TurnOn()
    MoveNumber(27)


def Solve7():
    TurnOn()
    TurnRight()
    MoveNumber(3)
    TurnLeft()
    MoveNumber(2)
    TurnLeft()
    MoveNumber(4)
    TurnRight()
    SingleDrop()
    TurnRight()
    Move()
    SingleDrop()
    MoveNumber(4)
    PickUp()
    TurnAround()
    SingleDrop()
    TurnAround()
    MoveNumber(4)
    TurnRight()
    MoveNumber(2)
    TurnLeft()
    MoveNumber(2)
    TurnRight()
    MoveNumber(3)
