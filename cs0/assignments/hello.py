#!/usr/bin/env python

"""Howard SYCS100 Programming Assignment 1: Hello

Tasks:
 0) Save the script as hello.py.
 1) Update these variables with your information.
    student_id should be an integer with your Howard student ID (no @ symbol).
 2) Set a message to be your welcoming message!
    In class this was "Hello, world!!" Yours *must* be different.
 3) Print your name and message to the output.
    It should be formatted *exactly* as follows: [FirstName LastName: Message]
    *Do not include the [] in your output.
 4) Run your code. And follow the instruction on the course site.

Notes
  - Use you real name or you won't get graded correctly!
  - Always use your @scs.howard.edu or @bison.howard.edu email in this class!
  - This is a individual project.  You must complete all work on your own.
  - Lines that start with # are comments. They will not be executed.
"""

first_name = 'Irie'
last_name = 'Grant'
message = 'Even if the morrow is barren of promises...Nothing shall forestall my return.'
student_id = 2733486
print first_name + ' ' + last_name + ':' + ' ' + message
