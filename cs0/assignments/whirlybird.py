"""Howard SYCS100 Programming Assignment 3: Whirlybird

Now is the time for you to practice writing a program *from scratch*!!

You are going to implement a whirlybird game. Whirlybird is that fortune-
telling game where your fortune is determined using a number and a color.

This project is intended to have you practice a few things:
    - Using both while and for loops
    - Writing a program from scratch
    - Defining and calling functions
    - Using raw_input
    - Slightly more advanced string manipulation
  

Grading Rubric:
    - Functionality: 100 pts (graded automatically)
    - Style: 50 pts (graded manually)

Notes:
    - This is an individual project. You must complete all work on your own.
    - Copy the current contents of this file as your starting point.
    - Follow directions *carefully*. The autograder isn't very tolerant.
    - Be sure to follow the coding standards.
    - You may define helper functions in this file, but be sure that you 
      definitely have all of the required functions and they are in the format
      described in the comments.
    - Please use appropriate fortunes. Some should be hopeful, some should be
      devastating, but all should be G-rated (i.e. safe for children).
"""

"""Global Variables

Define four variables called BLUE, GREEN, RED, and YELLOW that contain string 
values you can use to compare against user input (e.g. later on in the 
program, the user is going to input a color, like 'red' and you need to match 
that to RED).

Also, define a variable called EXIT that contains the string 'exit' (which is 
what the user enters when he wants to quit the program.

You should use these variables throughout the program rather than hardcoding 
'red', 'blue', 'green', 'yellow', or 'exit' anywhere else in the code.
"""
# Define variables here
BLUE = 'blue'
GREEN = 'green'
RED = 'red'
YELLOW = 'yellow'
EXIT = 'exit'

"""GetFortune Function

Define a function called GetFortune that takes one parameter named fortune_num. 
The parameter should be an integer between 1 and 8 (inclusive). For each valid 
value, the function should return a different fortune as a string. If the 
parameter's value is anything other than an integer between 1 and 8, the 
function should return the following string: 
  'Invalid fortune number.'
"""
# Define GetFortune here
def GetFortune(fortune_num):
    if fortune_num < 1 or fortune_num > 8:
        return 'Invalid fortune number.'
        
    if fortune_num == 1:
        return 'Yes. This is True.'

    if fortune_num == 2:
        return '#nah'

    if fortune_num == 3:
        return '..Not really'

    if fortune_num == 4:
        return 'Never in your wildest dreams'

    if fortune_num == 5:
        return 'That is impossible to determine'
        
    if fortune_num == 6:
        return 'Do you really need me to answer this? It is obvious'

    if fortune_num == 7:
        return 'no no no NO NO NO'

    if fortune_num == 8:
        return 'YAAAAAAAAAAAAAASSSSS'


"""GetColorSelection Function

Define a function called GetColorSelection that takes no parameters. The 
function should prompt the user to enter one of the colors defined at the 
beginning of the file. The user's input should not be case-sensitive. Only the 
four colors defined at the beginning of the program should be considered valid 
input and only valid values should be returned. If the user enters anything 
other than a valid color, you should notify the user that the input was invalid 
and prompt them to enter their color again. This process should continue until 
the user enters a valid color.

Note: see Python documentation to find a function that will convert strings 
      to lower case.

"""
# Define GetColorSelection here
def GetColorSelection():
    color_selection = raw_input('Type a color: red, blue, green or yellow\n')
    color_selection = color_selection.lower()
    color_list = [BLUE, GREEN, RED, YELLOW]

    if color_selection not in color_list:
        while True:
            color_selection = raw_input('\nInvalid color entered. Please try again. \nType a color: red, blue, green or yellow\n')
            color_selection = color_selection.lower()
            
            if color_selection in color_list:
                break
                
    return color_selection

"""GetNumberSelection Function

Define a function called GetNumberSelection that takes one parameter named 
use_even_numbers. The parameter will be a boolean value. The function should 
prompt the user to enter an interger and return their selection as an 
integer. If use_even_numbers is True, the user should be prompted to enter 2, 
4, 6, or 8. If use_even_numbers is False, the user should be prompted to 
input 1, 3, 5, or 7. No other input values should be allowed and only valid 
values should be returned. If the user enters anything other than a valid 
integer, you should notify the user that the input was invalid and prompt 
them to enter their number again. This process should continue until the user 
enters a valid number.
"""
# Define GetNumberSelection here

def GetNumberSelection(use_even_numbers):
    number = ''
    while use_even_numbers == True:
        number = (raw_input('\nChoose a number: 2, 4, 6 or 8\n'))
        number_list = ['2','4','6','8']
        if number in number_list:
            break
        else:
            while True:  
                number = raw_input('\nTry again\nChoose a number: 2, 4, 6 or 8\n')   
                if number in number_list:
                    break
        break
        
    while use_even_numbers == False:
        number = (raw_input('\nChoose a number: 1, 3, 5 or 7\n'))
        number_list = ['1','3','5','7']
        if number in number_list:
            break
        else:
            while True:  
                number = raw_input('\nTry again\nChoose a number: 1, 3, 5 or 7\n')   
                if number in number_list:
                    break
        break
    return int(number)
    
"""IsPrime Function

Define a function called IsPrime that takes one parameter named x. The 
parameter will be an integer. The function should return a boolean value: True 
if the number is prime, False otherwise. If x is not an integer, then the 
function should return None.

Notes:
    - 0 and 1 are *not* prime numbers.
    - Negative numbers are never prime.
    - Even though we only use 1-8 in this program, this function should work 
      for any integer.
"""
# Define IsPrime
def IsPrime(x):
    if x < 2:
        return False
    if x == 2:
        return True     
    else:
        for n in xrange(2, x):
            if x % n == 0:
                return False
            else:
                return True
                
"""Play Function

Define a function called Play that takes no parameters. The function should 
use the other functions you have defined to handle the overall logic of the 
whirlybird.
    1) The user should be prompted to enter a question for the whirlybird.
    2) If the user entered 'exit' (not case-sensitive), the program should exit.
    3) The user should be prompted to choose a color.
    4) If there are an even number of letters in the selected color, the user 
       should be prompted to select an even number. If there are an odd number 
       of letters in the color, they should be prompted to choose an odd number.
    5) If the number the user selected is prime, the user should be prompted to 
       enter an even number. If the selected number is not prime, the user 
       should be prompted to enter an odd number.
    6) This second number the user has selected should be used to determine the 
       fortune the user should receive.
    7) The user's original question and then the fortune (on separate lines) 
       should be printed out.
    8) Go back to step 1.
"""
# Define Play
def Play():
    while True:
        question = raw_input('\nEnter a yes or no question for the whirlybird! \n(type "exit" if you want to quit)\n\n')
        while question == '':
            question = raw_input('\nCome on, ask a question! \n(type "exit" if you want to quit)\n\n')
            
        if question.lower() == EXIT:
            break

        choice_one = GetNumberSelection(len(GetColorSelection()) % 2 == 0)
        choice_two = GetNumberSelection(IsPrime(choice_one))
        
        print 'To answer your question, ' + question + ':\n???????????????????????????????????????\n' + GetFortune(choice_two)

        
Play()  # DO NOT TOUCH THIS LINE!!
