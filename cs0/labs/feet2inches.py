#Irie Grant II 10.9.14 - Convert Feet to Inches
def feet2inches():
    height = raw_input('Enter height in feet as 3 consecutive digits i.e. 505 = 5 foot 5 inches\n')
    while len(height) != 3 or int(height) < 0 or int(height) % 100 > 11:
       height = raw_input('Incorrect Syntax, try again\n') 
    inches = int(height[1:])
    '''while inches > 11:
        height = raw_input('Try again. Make sure inches are less than 12\n')
        inches = int(height[1:])'''
    print 'Converting to inches....done'
    feet = int(height[0])
    return (feet * 12) + inches
    
print feet2inches()
