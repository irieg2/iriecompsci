# This is a comment.  A line begining with a # symbol is a comment.
# Comments are not executed by the python interpreter. Make sure you read all of
# the comments in this file carefully!


# Tasks:
#  1) Update these variables with your information.
#  2) Set a message to be your welcoming message! In class this was "Hello World".
#  3) Print your name and message to the output.
#  4) Run your code. And follow the instruction on the course site.

#Notes
#   - Use you real name or you won't get graded correctly!
#   - Always use your scs.howard.edu email in this class! I know you probably don't have one yet. Stay tuned.
#   - This is a individual project.  You must complete all work on your own.

# Modify the code below!

firstName = 'Irie'
lastName = 'Grant II'
email = 'irieg2@scs.howard.edu'
message = 'Even if the morrow is barren of promises..\nNothing shall forestall my return.'
_ = ' '
print'\n------------------\n' + firstName + _ + lastName+  '\n' + email + '\n' + message + '\n' + 'SYCS 100!'



