minHeight = raw_input("-"*80+"\nWhat is the minimum height for the ride?\n"+"-"*80+"\n")
height=raw_input("-"*80+"\nWhat is your height in feet?\n"+"-"*80+"\n")
def waiver():
	decision = raw_input("-"*80+"\nYou're not tall enough, but if you sign this waiver you can get on anyway. Will you sign? [y/n]\n"+"-"*80+"\n")
	return decision == "y"
def canRide(mH,gH):
	return int(gH) >= int(mH)	
if canRide(minHeight,height) == True:
	print "-"*80+"\nYou can ride :D"+"-"*80+"\n"
else:
	if waiver() == True:
		print "-"*80+"\nEnjoy your ride :D\n"+"-"*80+"\n"	
	else:
		print "-"*80+"\nSee ya later!\n"+"-"*80+"\n"
